# -*- coding:utf-8 -*-
import argparse
import io
import logger
import matplotlib.pyplot as plt
import numpy as np
import os
import platform
import requests
import sys
import torch
import zipfile

from collections import deque
from agent import Agent
from progressbar import ProgressBar
from unityagents import UnityEnvironment

MAX_EPISODES = [2000]
SCORES_WINDOW = 100
MAX_STEPS = 1000
MIN_SCORE = 13.0

AWS_ZIP = { 
    'Darwin': 'Banana',
    'Linux': 'Banana_Linux_NoVis',
    'Windows': 'Banana_Windows_x86_64'
}

def check_not_null(*args):
    assert (len(args) > 0)
    for arg in args:
        assert (arg is not None) 

def get_unity_env_path(download=True):
    """ 
        Gets the environment's executable path.
        
        Params
        ======
        download (bool): whether to download the AWS .zip file
        
    """
    system_name = platform.system()
    
    try:
        zip_name = AWS_ZIP[system_name]
    except:
        raise ValueError('Unknown OS \'%s\'' % system_name)

    if system_name == 'Linux':
        extension = platform.machine()
    elif system_name == 'Windows':
        extension = 'exe'
    elif system_name == 'Darwin':
        extension = 'app'
    
    path = os.path.join(zip_name, 'Banana.%s' % extension)
    
    if not os.path.exists(path) and download:
        url = 'https://s3-us-west-1.amazonaws.com/udacity-drlnd/P1/Banana/%s.zip' % zip_name
        logger.info("Downloading Unity environment from \'%s\'... please wait" % url)
        request = requests.get(url, stream=True)
        zip = zipfile.ZipFile(io.BytesIO(request.content))
        logger.info("Extracting... please wait")
        zip.extractall()
    
    return path

def plot_scores(scores, output):
    """
        Plot scores to a file.
        
        Params
        ======
        scores (array): array of scores
        output (str): outpuf file name
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.plot(np.arange(len(scores)), scores)
    plt.ylabel('Score')
    plt.xlabel('Episode #')
    plt.grid(True, which='major', linewidth=0.25)
    plt.grid(True, which='minor', linewidth=0.1)
    plt.title('DQN Banana collector agent average scores')
    plt.savefig(output)
    plt.clf()


#region exercise
class Project:
    """ 
        Class that represents the navigation project
    """

    def __init__(self, name=get_unity_env_path(), args=None):
        """
            Constructor.
            
            Params
            ======
            name (str): path to environment executable
            args (dict): optional arguments
        """
        check_not_null(name)
        
        _seed = (1 if args is None else args.seed)
        self.env = UnityEnvironment(file_name=name, seed=_seed)
        self.brain_name = self.env.brain_names[0]
        self.brain = self.env.brains[self.brain_name]
        self.action_size = self.brain.vector_action_space_size
        self.agent = Agent(state_size=37, action_size=self.action_size, seed=_seed)
        self.args = args
    
    def _build_output_filename(self, prefix, episodes, eps):
        return ('%s-e%06d-eps%.4f-%.4f.png' % (prefix, episodes, self.args.eps, eps))
    
    def _take_random_actions(self):
        """
            Takes random actions in the environment.
        """
        env_info = self.env.reset(train_mode=False)[self.brain_name]
        
        S = env_info.vector_observations[0]
        score = 0
        count = 0
        while True:
            A = np.random.randint(self.action_size)
            count += 1
            env_info = self.env.step(A)[self.brain_name]
            S_prime = env_info.vector_observations[0]
            R = env_info.rewards[0]
            done = env_info.local_done[0]
            score += R
            S = S_prime
            if done:
                break
        
        self.env.close()
        
        logger.info("Taking %d random actions in the environment lead to a score of %.2f" % (count, score))
    
    def _train(self):
        """
            Trains the collector agent.
        """
        (eps, eps_decay, eps_min, plot, model) = \
            (self.args.eps, self.args.eps_decay, self.args.eps_min, self.args.plot, self.args.model)
        
        for n_episodes in MAX_EPISODES:
            
            logger.info("Training agent for %d episodes (max_steps = %d)..." % (n_episodes, MAX_STEPS))
            
            scores = []
            scores_window = deque(maxlen=SCORES_WINDOW)
            
            msgs = []
            msgs.append("Last %d scores" % SCORES_WINDOW)
            msgs.append("#   \tavg score")
            msgs.append("----\t---------")
            
            bar = ProgressBar(max_value=n_episodes, redirect_stdout=True)
            for idx in range(1, n_episodes + 1):
                score = 0
                env_info = self.env.reset(train_mode=True)[self.brain_name]
                S = env_info.vector_observations[0] 
                for step in range(MAX_STEPS):
                    # choose and action A from state S using epsilong-greedy policy
                    A = self.agent.act(S, eps)
                    # take action A, observe reward R and next input frame
                    env_info = self.env.step(A)[self.brain_name]
                    R = env_info.rewards[0]
                    done = env_info.local_done[0]
                    # prepare next state S_prime
                    S_prime = env_info.vector_observations[0]
                    # store experience tuple in replay memory
                    self.agent.step(S, A, R, S_prime, done)
                    score += R
                    # and prepare the next iteration
                    S = S_prime
                    if done:
                        break
                
                scores_window.append(score) 
                scores.append(score) 
                eps = max(eps_min, eps_decay*eps)
                
                if idx % SCORES_WINDOW == 0:
                    msgs.append('%4d\t%.7f' % (idx, np.mean(scores_window)))
                
                #if np.mean(scores_window) >= MIN_SCORE:
                #    msgs.append('%4d\t%.7f' % (idx - SCORES_WINDOW, np.mean(scores_window)))
                #    torch.save(self.agent.qnetwork_local.state_dict(), model)
                #    break
                
                bar.update(bar.value + 1)
                
            torch.save(self.agent.qnetwork_local.state_dict(), model)
            
            bar.finish()
            
            for msg in msgs: 
                logger.info(msg)
            
            if plot:
                plot_scores(scores, self._build_output_filename('scores', n_episodes, eps))
    
    def _test(self):
        """
            Tests the collector agent.
        """
        self.agent.qnetwork_local.load_state_dict(torch.load(self.args.model))
        
        n_episodes = 100
        
        scores = []
        bar = ProgressBar(max_value=n_episodes, redirect_stdout=True)
        for idx in range(1, n_episodes + 1):
            
            env_info = self.env.reset(train_mode=False)[self.brain_name] 
            S = env_info.vector_observations[0]
            score = 0
            
            while True:
                A = self.agent.act(S)
                env_info = self.env.step(A)[self.brain_name]
                S_prime = env_info.vector_observations[0]
                reward = env_info.rewards[0]
                done = env_info.local_done[0]
                score += reward
                S = S_prime
                if done:
                    scores.append(score)
                    break
            
            bar.update(bar.value + 1)
        
        bar.finish()
        
        logger.info("#   \tscore")
        logger.info("----\t---------")
        for idx, score in enumerate(scores):
            logger.info('%4d\t%.7f' % ((idx + 1), score))
            
        logger.info("Mean score: %.7f" % np.mean(scores))
    
    def run(self):
        """
            Runs the project.
        """
        (random, train, test) = (self.args.random, self.args.train, self.args.test)
        if random:
            self._take_random_actions()
        elif train:
            self._train()
        elif test:
            self._test()
        else:
            raise ValueError()
#endregion

#region main
def main(parser):
    """ 
        Entry point.
    """
    args = parser.parse_args()
    if args.cmd == 'vanilla':
        project = Project(args=args)
    else:
        parser.print_help()
        return
    project.run()

def _build_argparser():
    
    parser = argparse.ArgumentParser(description='DRLND navigation project.\n')
    parser.add_argument('--gamma', type=float, default=1.0, help='gamma discounting factor (defaults to 1.0)')
    parser.add_argument('--eps', type=float, default=1.0, help='epsilon (defaults to 1.0)')
    parser.add_argument('--eps-decay', type=float, default=0.995, help='epsilon decay factor (defaults to 0.995)')
    parser.add_argument('--eps-min', type=float, default=0.05, help='minimum epsilon value(defaults to 0.05)')
    parser.add_argument('--seed', type=int, default=1, help='seed (defaults to 1)')
    parser.add_argument('--plot', action='store_true', help='plot graphs (defaults to false)')
    parser.add_argument('--model', type=str, default='model.pt', help='checkpoint file name (default to \'model.pt\')')
    
    cmd_sp = parser.add_subparsers(dest='cmd')
    
    mc_p = cmd_sp.add_parser('vanilla', help='uses a Vanilla DQN')
    mc_grp = mc_p.add_mutually_exclusive_group(required=True)
    mc_grp.add_argument('-random', action='store_true', help='Takes random actions')
    mc_grp.add_argument('-train', action='store_true', help='Trains the agent and saves resulting model')
    mc_grp.add_argument('-test', action='store_true', help='Tests a pre-trained model')
    
    return parser

if __name__ == "__main__":
    main(_build_argparser())
    sys.exit(0)
#endregion