# Introduction

Please take a look at [README](README.md) file for a brief description of the project.

Now take a look at how an trained agent would interact with the environment once training is done using the parameters described in the following sections:

![Trained agent interacting with the environment.](img/agent-e002000-eps1.0000-0.0780.gif)

# Learning algorithm

We use a simple version of the Deep Q-Learning algorithm proposed in this [research paper](https://storage.googleapis.com/deepmind-media/dqn/DQNNaturePaper.pdf). 

The following code snippet represents the core of the training algorithm, which can be found in its entirety in `main.py` script.

```python
    ...
    for idx in range(1, n_episodes + 1):                  # see hyperparameters section
        score = 0                                         # total score of an episode
        env_info = env.reset(train_mode=True)[brain_name] # reset the environment
        S = env_info.vector_observations[0]               # how does the environment look like?
        for step in range(MAX_STEPS):                     # see hyperparameters section
            A = self.agent.act(S, eps)                    # take an action using a epsilon-greedy policy (see hyperparameters section)
            env_info = env.step(A)[self.brain_name]       #    and inform the environment 
            S_prime = env_info.vector_observations[0]     # how does the environment look like now?
            R = env_info.rewards[0]                       # how big was the reward?
            done = env_info.local_done[0]                 # is my episode done?
            agent.step(S, A, R, S_prime, done)            # actually move the agent
            score += R                                    #     collect the reward
            S = S_prime                                   #     and move on to the next state
            if done:
                break
    ...
``` 

## Network architecture

Our vanilla neural network can be found in `model.py` script. 

It uses a `37x64x64x4` architecture comprising the following layers:
- `fc1` as a fully connected layer with input size of 37 (representing the state size) and output size of 64
- `fc2` as a fully connected layer with input size of 64 and output size of 64
- `fc3` as a fully connected layer with input size of 64 and output size of 4

`fc2` and `fc3` are both activated via ReLU.

## Hyperparameters

Our vanilla implementation uses the following hyperparameters:
- Number of episodes (`n_episodes`): $`2000`$
- Maximum steps per episode (`MAX_STEPS`): $`1000`$
- Epsilon-greed policy: 
    - epsilon (`eps`): $`\epsilon = 1.0`$
    - decay rate: $`0.995`$
    - minimum epsilon $`0.05`$
- Memory buffer size: $`1e5`$
- Memory batch size: $`64`$
- Soft update of target parameters: $`\tau = 1e-3`$
- Learning rate: $`\alpha = 5e-4`$
- Discount factor: $`\gamma = 0.99`$
- Frame skipping: 4 (which means we select actions on every 4th frame)

# Plot of rewards

Training an agent using the parameters should produce a plot of rewards like the following:

![Plot of results after 2000 episodes with epsilon starting at 1.0 and ending at 0.078.](img/scores-e002000-eps1.0000-0.0780.png)

# Ideas for future work

Ideas for future work include:
- Expand `agent.py` and `model.py` scripts to experiment with different network architectures. 
These experiments, that could be done via some kind of grid search, would aim to answer questions like:
    - How well would an alternative number of layers and neurons/cells perform? Scores would increase or decrease compared to the default `37x64x64x4` architeture layout?
    - How well would an alternative activation unit perform compared to ReLU?
    - How well would an alternative optimizer perform compared to Adam?
    - What about alternative hyperparameter values (gamma, tau, memory size and windows, learning rate, batch size...)? 
- Expand `model.py`, implement and evaluate:
    - [double DQN](https://arxiv.org/abs/1509.06461)
    - [dueling DQN](https://arxiv.org/abs/1511.06581)
    - [prioritized experience replay](https://arxiv.org/abs/1511.059520)
    - CNN to learn actions from pixels