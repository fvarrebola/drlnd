# Project details

For this project, we will train an agent to navigate and collect yellow bananas in a large, square world.  

A reward of +1 is provided for collecting a yellow banana, and a reward of -1 is provided for collecting a blue banana. Thus, the goal of your agent is to collect as many yellow bananas as possible while avoiding blue bananas.  

The state space has 37 dimensions and contains the agent's velocity, along with ray-based perception of objects around agent's forward direction. Given this information, the agent has to learn how to best select actions. 

Four discrete actions are available, corresponding to:
- **`0`** - move forward.
- **`1`** - move backward.
- **`2`** - turn left.
- **`3`** - turn right.

The task is episodic, and in order to solve the environment, your agent must get an average score of +13 over 100 consecutive episodes.

# Getting started

All you have to do is to create a Python virtual environment, activate it and install the requirements using `pip`.
```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

Please note that if you are running on Windows you may have to install [PyTorch](https://pytorch.org) manually according to the instructions found [here](https://pytorch.org/get-started/locally/).

# Instructions

Once your environment is set, navigate to `src/` folder and run `python main.py`. 

You should see the following output:
```
usage: main.py [-h] [--gamma GAMMA] [--eps EPS] [--eps-decay EPS_DECAY]
               [--eps-min EPS_MIN] [--seed SEED] [--plot] [--model MODEL]
               {vanilla} ...

DRLND navigation project.

positional arguments:
  {vanilla}
    vanilla             uses a Vanilla DQN

optional arguments:
  -h, --help            show this help message and exit
  --gamma GAMMA         gamma discounting factor (defaults to 1.0)
  --eps EPS             epsilon (defaults to 1.0)
  --eps-decay EPS_DECAY
                        epsilon decay factor (defaults to 0.995)
  --eps-min EPS_MIN     minimum epsilon value(defaults to 0.05)
  --seed SEED           seed (defaults to 1)
  --plot                plot graphs (defaults to false)
  --model MODEL         checkpoint file name (default to 'model.pt')
```

Please note that you do not have to worry about downloading Unity's environment since the `main.py` script will do that for you.

## Available actions

Run `python main.py vanilla -h` to see which actions you can take.
```
usage: main.py vanilla [-h] (-random | -train | -test)

optional arguments:
  -h, --help  show this help message and exit
  -random     Takes random actions
  -train      Trains the agent and saves resulting model
  -test       Tests a pre-trained model
```

### Random actions

If you would like to take random actions in the environment then run `python main.py vanilla -random`.

The output should be something like:
```
2019-03-20 16:52:07,336 INFO     Taking 300 random actions in the environment lead to a score of -2.00
```

### Training the agent

If you would like to train the collector agent then run `python main.py vanilla -train`.

You should see an output like the following:
```
2019-03-20 09:30:44,328 INFO     Downloading Unity environment from 'https://...'... please wait
2019-03-20 09:31:22,974 INFO     Extracting... please wait
...
2019-03-20 09:31:31,867 INFO     Training agent for 2000 episodes (max_steps = 1000)...
100% (2000 of 2000) |##########| Elapsed Time: 1:35:00 Time:  1:35:00
2019-03-20 11:06:37,030 INFO     Last 100 scores
2019-03-20 11:06:37,038 INFO     #      avg score
2019-03-20 11:06:37,052 INFO     ----   ---------
2019-03-20 11:06:37,076 INFO      100   1.2300000
2019-03-20 11:06:37,082 INFO      200   4.7800000
2019-03-20 11:06:37,088 INFO      300   7.7200000
...
2019-03-20 11:06:37,113 INFO     1800   15.5200000
2019-03-20 11:06:37,115 INFO     1900   15.2400000
2019-03-20 11:06:37,116 INFO     2000   14.3300000
```

Training saves the resulting model to `model.pt` file. 

Training also plots scores to `scores-e[#-of-episodes]-eps[epsilon]-[min-epsilon].png` where:
- `#-of-episodes` indicates the number of episodes (that defaults to `2000`)
- `epsilon` indicates the starting value of epsilon (that defaults to `1.0`) 
- `min-epsilon` the minimum value of epsilon (that defaults to `0.1`). A typical plot would like this:

![Plot of results after 2000 episodes with epsilon starting at 1.0 and ending at 0.078.](img/scores-e002000-eps1.0000-0.0780.png)


### Testing the agent

If you would like to test the collector agent then run `python main.py vanilla -test`.

After a while you should see an output like:

```
100% (100 of 100) |##########| Elapsed Time: 0:51:40 Time:  0:51:40
2019-03-20 15:11:49,111 INFO     #      score
2019-03-20 15:11:49,127 INFO     ----   ---------
2019-03-20 15:11:49,128 INFO        1   17.0000000
2019-03-20 15:11:49,129 INFO        2   19.0000000
2019-03-20 15:11:49,130 INFO        3   18.0000000
...
2019-03-20 15:11:49,313 INFO       98   11.0000000
2019-03-20 15:11:49,315 INFO       99   21.0000000
2019-03-20 15:11:49,316 INFO      100   14.0000000
2019-03-20 15:11:49,354 INFO     Mean score: 15.6300000
```

Please note that testing requires a valid  `model.pt` file. 
