# -*- coding:utf-8 -*-
import argparse
import gym
import logger
import matplotlib.pyplot as plt
import numpy as np
import sys

from collections import defaultdict
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.axes_grid1 import make_axes_locatable
from params import check_not_null
from progressbar import ProgressBar

MAX_EPISODES = [1000, 5000, 10000, 50000, 100000, 200000, 300000, 400000, 500000]

#region plot_utils.py
def plot_blackjack_values(V, output):

    def get_Z(x, y, usable_ace):
        if (x,y,usable_ace) in V:
            return V[x,y,usable_ace]
        else:
            return 0

    def get_figure(usable_ace, ax):
        x_range = np.arange(11, 22)
        y_range = np.arange(1, 11)
        X, Y = np.meshgrid(x_range, y_range)
        
        Z = np.array([get_Z(x,y,usable_ace) for x,y in zip(np.ravel(X), np.ravel(Y))]).reshape(X.shape)

        surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=plt.cm.coolwarm, vmin=-1.0, vmax=1.0)
        ax.set_xlabel('Player\'s Current Sum')
        ax.set_ylabel('Dealer\'s Showing Card')
        ax.set_zlabel('State Value')
        ax.view_init(ax.elev, -120)

    fig = plt.figure(figsize=(20, 20))
    ax = fig.add_subplot(211, projection='3d')
    ax.set_title('Usable Ace')
    get_figure(True, ax)
    ax = fig.add_subplot(212, projection='3d')
    ax.set_title('No Usable Ace')
    get_figure(False, ax)
    plt.savefig(output)

def plot_policy(policy, output):

    def get_Z(x, y, usable_ace):
        if (x,y,usable_ace) in policy:
            return policy[x,y,usable_ace]
        else:
            return 1

    def get_figure(usable_ace, ax):
        x_range = np.arange(11, 22)
        y_range = np.arange(10, 0, -1)
        X, Y = np.meshgrid(x_range, y_range)
        Z = np.array([[get_Z(x, y, usable_ace) for x in x_range] for y in y_range])
        surf = ax.imshow(Z, cmap=plt.get_cmap('Pastel2', 2), vmin=0, vmax=1, extent=[10.5, 21.5, 0.5, 10.5])
        plt.xticks(x_range)
        plt.yticks(y_range)
        plt.gca().invert_yaxis()
        ax.set_xlabel('Player\'s Current Sum')
        ax.set_ylabel('Dealer\'s Showing Card')
        ax.grid(color='w', linestyle='-', linewidth=1)
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.1)
        cbar = plt.colorbar(surf, ticks=[0,1], cax=cax)
        cbar.ax.set_yticklabels(['0 (STICK)','1 (HIT)'])
            
    fig = plt.figure(figsize=(15, 15))
    ax = fig.add_subplot(121)
    ax.set_title('Usable Ace')
    get_figure(True, ax)
    ax = fig.add_subplot(122)
    ax.set_title('No Usable Ace')
    get_figure(False, ax)
    plt.savefig(output)
#endregion

#region exercise
class Exercise:
    def __init__(self, env, args):
        check_not_null(env)
        self.env = env
        self.nA = self.env.action_space.n
        self.args = args

    def _build_output_filename(self, prefix, episodes):
        return ('ch08/ch08-%s-e%06d-a%.2f-g%.2f-e%.2f.png' % \
            (prefix, episodes, self.args.alpha, self.args.gamma, self.args.eps))

    def _blackjack_pdf(state):
        return ([.8, .2] if state[0] > 18 else [.2, .8])
    
    def _generate_episode_using_pd(self, pdf=_blackjack_pdf):
        ''' Generates an episode using a given probability distribution function '''
        check_not_null(pdf)
        
        episode = []
        state = self.env.reset()
        while True:
            probs = pdf(state)
            assert(round(sum(probs), 2) == 1.00)
            action = np.random.choice(np.arange(self.nA), p=probs)
            next_state, reward, done, info = self.env.step(action)
            episode.append((state, action, reward))
            state = next_state
            if done:
                break
        if logger.is_debug_enabled():
            for idx, _step in enumerate(episode):
                (state, action, reward) = _step
                logger.debug("state: %s, action: %s, reward: %s" % (state, action, reward)) 
        
        return episode

    def _mc_prediction(self):
        ''' Runs the Monte Carlo prediction algorithm to compute Q '''
        (gamma, plot) = (self.args.gamma, self.args.plot)
        
        for episodes in MAX_EPISODES:
            returns_sum = defaultdict(lambda: np.zeros(self.nA))
            N = defaultdict(lambda: np.zeros(self.nA))
            Q = defaultdict(lambda: np.zeros(self.nA))
            bar = ProgressBar(max_value=episodes, redirect_stdout=True)
            for idx in range(1, episodes + 1):
                episode = self._generate_episode_using_pd()
                states, actions, rewards = zip(*episode)
                discounts = np.array([gamma**i for i in range(len(rewards)+1)])
                for jdx, state in enumerate(states):
                    #if (first_visit and states.index(state) < i):
                    #    continue
                    returns_sum[state][actions[jdx]] += sum(rewards[jdx:]*discounts[:-(1+jdx)])
                    N[state][actions[jdx]] += 1.0
                    Q[state][actions[jdx]] = returns_sum[state][actions[jdx]] / N[state][actions[jdx]]
                bar.update(bar.value + 1)
            bar.finish()
        
            if plot:
                V_to_plot = dict( \
                    (k,(k[0]>18)*(np.dot([0.8, 0.2],v)) + (k[0]<=18)*(np.dot([0.2, 0.8],v))) \
                        for k, v in Q.items())
                plot_blackjack_values(V_to_plot, self._build_output_filename('mc-values', episodes))

    def _get_probs(self, Q_s, epsilon, nA):
        ''' Obtains the action probabilities corresponding to epsilon-greedy policy '''
        policy_s = np.ones(nA) * epsilon / nA
        best_a = np.argmax(Q_s)
        policy_s[best_a] = 1 - epsilon + (epsilon / nA)
        return policy_s

    def _update_Q(self, episode, Q, alpha, gamma):
        ''' Updates the action-value function estimate using the most recent episode '''
        states, actions, rewards = zip(*episode)
        discounts = np.array([gamma**i for i in range(len(rewards)+1)])
        for i, state in enumerate(states):
            old_Q = Q[state][actions[i]] 
            Q[state][actions[i]] = old_Q + alpha*(sum(rewards[i:]*discounts[:-(1+i)]) - old_Q)
        return Q

    def _generate_episode_from_Q(self, Q, epsilon):
        ''' Generates an episode from following the epsilon-greedy policy '''
        episode = []
        state = self.env.reset()
        while True:
            action = np.random.choice( \
                np.arange(self.nA), \
                p=self._get_probs(Q[state], epsilon, self.nA)) if state in Q else self.env.action_space.sample()
            next_state, reward, done, info = self.env.step(action)
            episode.append((state, action, reward))
            state = next_state
            if done:
                break
        return episode

    def _mc_const_alpha_control(self):
        ''' Runs the Monte Carlo alpha-control algorithm '''
        (alpha, gamma, eps, eps_decay, eps_min, plot) = \
            (self.args.alpha, self.args.gamma, self.args.eps, self.args.eps_decay, self.args.eps_min, self.args.plot)
        
        for episodes in MAX_EPISODES:
            Q = defaultdict(lambda: np.zeros(self.nA))
            epsilon = eps
            bar = ProgressBar(max_value=episodes, redirect_stdout=True)
            for idx in range(1, episodes + 1):
                epsilon = max(epsilon*eps_decay, eps_min)
                episode = self._generate_episode_from_Q(Q, epsilon)
                Q = self._update_Q(episode, Q, alpha, gamma)
                bar.update(bar.value + 1)
            bar.finish()
        
            if plot:
                V = dict((k, np.max(v)) for k, v in Q.items())
                plot_blackjack_values(V, self._build_output_filename('control-values', episodes))
                policy = dict((k, np.argmax(v)) for k, v in Q.items())
                plot_policy(policy, self._build_output_filename('control-policy', episodes))

    def run(self):
        (naive, control) = (self.args.naive, self.args.control)
        if naive:
            self._mc_prediction()
        elif control:
            self._mc_const_alpha_control()
        else:
            raise ValueError()
#endregion