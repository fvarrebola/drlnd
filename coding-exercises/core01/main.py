# -*- coding:utf-8 -*-
import argparse
import gym
import sys

import logger
import ch08
import ch09
import ch10

def _make_gym_env(name):
    ''' Initializes the environment '''
    check_not_null(name)
    
    env = gym.make(name)
    logger.info('Environment \'%s\'' % name)
    logger.info('Observation space: %s' % env.observation_space)
    logger.info('Action space: %s' % env.action_space)
    
    return env

#region main
def main(parser):
    ''' Entry point '''
    args = parser.parse_args()
    if args.cmd == 'mc':
        exercise = ch08.Exercise(gym.make('Blackjack-v0'), args)
    elif args.cmd == 'td':
        exercise = ch09.Exercise(gym.make('CliffWalking-v0'), args)
    elif args.cmd == 'taxi':
        exercise = ch10.Exercise(gym.make('Taxi-v2'), args)        
    else:
        parser.print_help()
        return
    exercise.run()


def _build_argparser():
    
    parser = argparse.ArgumentParser(description='DRLND coding exercises (from the Introduction chapter).\n')
    parser.add_argument('--alpha', type=float, default=0.02, help='alpha (defaults to 0.02)')
    parser.add_argument('--gamma', type=float, default=1.0, help='gamma discounting factor (defaults to 1.0)')
    parser.add_argument('--eps', type=float, default=1.0, help='epsilon (defaults to 1.0)')
    parser.add_argument('--eps-decay', type=float, default=0.99999, help='epsilon decay factor (defaults to 0.99999)')
    parser.add_argument('--eps-min', type=float, default=0.05, help='minimum epsilon value(defaults to 0.05)')
    parser.add_argument('--plot', action='store_true', help='whether to plot graphs (defaults to false)')
    
    
    cmd_sp = parser.add_subparsers(dest='cmd')
    mc_p = cmd_sp.add_parser('mc', help='runs Monte Carlo simulations')
    mc_grp = mc_p.add_mutually_exclusive_group(required=True)
    mc_grp.add_argument('-naive', action='store_true', help='Naive approach')
    mc_grp.add_argument('-control', action='store_true', help='Alpha control')
    
    td_p = cmd_sp.add_parser('td', help='runs TD Learning (SARSA and Q-Learning) simulations')
    td_grp = td_p.add_mutually_exclusive_group(required=True)
    td_grp.add_argument('-sarsa', action='store_true', help='SARSA')
    td_grp.add_argument('-q-learning', action='store_true', help='Q-Learning')
    td_grp.add_argument('-expected-sarsa', action='store_true', help='Expected SARSA')
    

    taxi_p = cmd_sp.add_parser('taxi', help='runs Taxi simulations')
    
    return parser



if __name__ == "__main__":
    main(_build_argparser())
    sys.exit(0)
#endregion