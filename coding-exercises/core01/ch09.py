# -*- coding:utf-8 -*-
import argparse
import gym
import logger
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import sys
import unittest 

from collections import defaultdict, deque
from params import check_not_null
from progressbar import ProgressBar

sns.set_style("white")

MAX_EPISODES = [1000, 2000, 3000, 4000, 5000]
PLOT_EVERY = 100

#region plot_utils.py
def plot_values(V, output):
    # reshape the state-value function
    V = np.reshape(V, (4,12))
    # plot the state-value function
    fig = plt.figure(figsize=(15,5))
    ax = fig.add_subplot(111)
    im = ax.imshow(V, cmap='cool')
    for (j,i),label in np.ndenumerate(V):
        ax.text(i, j, np.round(label,3), ha='center', va='center', fontsize=14)
    plt.tick_params(bottom='off', left='off', labelbottom='off', labelleft='off')
    plt.title('State-Value Function')
    plt.savefig(output)
    plt.clf()

def plot_performance(title, data, output):
    labels = []
    markers = ['o', 's', '^', 'x', '+']
    for idx, series in enumerate(data):
        length = len(series)
        episodes = length * 100
        labels.append('%d episodes - max R=%.2f' % (episodes, np.max(series)))
        plt.grid(True, which='major', linewidth=0.25)
        plt.grid(True, which='minor', linewidth=0.1)
        plt.plot(np.linspace(0, episodes, length, endpoint=False), np.asarray(series), marker=markers[idx], linewidth=.5, markersize=4, fillstyle='none')

    plt.legend(labels)
    plt.title(title)
    plt.xlabel('Episode Number')
    plt.ylabel('Average Reward (Over Next %d Episodes)' % PLOT_EVERY)
    plt.savefig(output)
    plt.clf()
#endregion

#region check_test.py
V_opt = np.zeros((4,12))
V_opt[0:13][0] = -np.arange(3, 15)[::-1]
V_opt[0:13][1] = -np.arange(3, 15)[::-1] + 1
V_opt[0:13][2] = -np.arange(3, 15)[::-1] + 2
V_opt[3][0] = -13

pol_opt = np.hstack((np.ones(11), 2, 0))

V_true = np.zeros((4,12))
for i in range(3):
    V_true[0:13][i] = -np.arange(3, 15)[::-1] - i
V_true[1][11] = -2
V_true[2][11] = -1
V_true[3][0] = -17

def get_long_path(V):
    return np.array(np.hstack((V[0:13][0], V[1][0], V[1][11], V[2][0], V[2][11], V[3][0], V[3][11])))

def get_optimal_path(policy):
    return np.array(np.hstack((policy[2][:], policy[3][0])))

class Tests(unittest.TestCase):
    def td_prediction_check(self, V):
        to_check = get_long_path(V)
        soln = get_long_path(V_true)
        np.testing.assert_array_almost_equal(soln, to_check)

    def td_control_check(self, policy):
        to_check = get_optimal_path(policy)
        np.testing.assert_equal(pol_opt, to_check)

check = Tests()

def run_check(check_name, func):
    getattr(check, check_name)(func)
#endregion

#region exercise
class Exercise:
    def __init__(self, env, args):
        check_not_null(env)
        self.env = env
        self.nA = self.env.action_space.n
        self.args = args

    def _build_output_filename(self, prefix, episodes=0):
        return ('ch09/ch09-%s-e%06d-a%.2f-g%.2f-e%.2f.png' % \
            (prefix, episodes, self.args.alpha, self.args.gamma, self.args.eps))

    def _update_Q(self, Qsa, Qsa_next, reward, alpha, gamma):
        """ updates the action-value function estimate using the most recent time step """
        return Qsa + (alpha * (reward + (gamma * Qsa_next) - Qsa))

    def _epsilon_greedy_probs(self, Q_s, i_episode, eps=None):
        """ obtains the action probabilities corresponding to epsilon-greedy policy """
        epsilon = 1.0 / i_episode
        if eps is not None:
            epsilon = eps
        policy_s = np.ones(self.nA) * epsilon / self.nA
        policy_s[np.argmax(Q_s)] = 1 - epsilon + (epsilon / self.nA)
        return policy_s

    def _sarsa(self):
        
        (alpha, gamma, plot) = (self.args.alpha, self.args.gamma, self.args.plot)
        
        data = []
        for max in MAX_EPISODES:
            scores = deque(maxlen=PLOT_EVERY)
            series = deque(maxlen=max)
            Q = defaultdict(lambda: np.zeros(self.nA))
            bar = ProgressBar(max_value=max, redirect_stdout=True)
            for idx in range(1, max + 1):
                score = 0
                S = self.env.reset()
                P = self._epsilon_greedy_probs(Q[S], idx)
                A = np.random.choice(np.arange(self.nA), p=P)
                for t_step in np.arange(300):
                    S_prime, R, done, info = self.env.step(A)
                    score += R
                    if done:
                        Q[S][A] = self._update_Q(Q[S][A], 0, R, alpha, gamma)
                        scores.append(score)
                        break
                    P = self._epsilon_greedy_probs(Q[S_prime], idx)
                    A_prime = np.random.choice(np.arange(self.nA), p=P)
                    Q[S][A] = self._update_Q(Q[S][A], Q[S_prime][A_prime], R, alpha, gamma)
                    S = S_prime
                    A = A_prime
                if (idx % PLOT_EVERY == 0):
                    series.append(np.mean(scores))
                
                bar.update(bar.value + 1)
                
            data.append(series)
            bar.finish()
        
            #P = np.array([np.argmax(Q[key]) if key in Q else -1 for key in np.arange(48)]).reshape(4,12)
            #run_check('td_control_check', P)
        
        if plot:
            title = 'SARSA (alpha = %.2f, gamma = %.2f)' % (alpha, gamma)
            output = self._build_output_filename('sarsa-performance')
            plot_performance(title, data, output)
            V = ([np.max(Q[key]) if key in Q else 0 for key in np.arange(48)])
            plot_values(V, self._build_output_filename('sarsa-values', max))

    def _q_learning(self):
    
        (alpha, gamma, plot) = (self.args.alpha, self.args.gamma, self.args.plot)
        
        data = []
        for max in MAX_EPISODES:
            scores = deque(maxlen=PLOT_EVERY)
            series = deque(maxlen=max)
            Q = defaultdict(lambda: np.zeros(self.nA))
            bar = ProgressBar(max_value=max, redirect_stdout=True)
            for idx in range(1, max + 1):
                score = 0
                S = self.env.reset()
                while True:
                    P = self._epsilon_greedy_probs(Q[S], idx)
                    A = np.random.choice(np.arange(self.nA), p=P)
                    S_prime, R, done, info = self.env.step(A)
                    score += R
                    Q[S][A] = self._update_Q(Q[S][A], np.max(Q[S_prime]), R, alpha, gamma)
                    S = S_prime
                    if done:
                        scores.append(score)
                        break
                if (idx % PLOT_EVERY == 0):
                    series.append(np.mean(scores))
                
                bar.update(bar.value + 1)
            data.append(series)
            bar.finish()
            
            #P = np.array([np.argmax(Q[key]) if key in Q else -1 for key in np.arange(48)]).reshape(4,12)
            #run_check('td_control_check', P)
        
            if plot:
                title = 'Q-Learning (alpha = %.2f, gamma = %.2f)' % (alpha, gamma)
                output = self._build_output_filename('q_learning-performance')
                plot_performance(title, data, output)
                V = ([np.max(Q[key]) if key in Q else 0 for key in np.arange(48)])
                plot_values(V, self._build_output_filename('q_learning-values', max))

    def _expected_sarsa(self):
    
        (alpha, gamma, plot) = (self.args.alpha, self.args.gamma, self.args.plot)
        
        data = []
        for max in MAX_EPISODES:
            scores = deque(maxlen=PLOT_EVERY)
            series = deque(maxlen=max)
            Q = defaultdict(lambda: np.zeros(self.nA))
            bar = ProgressBar(max_value=max, redirect_stdout=True)
            for idx in range(1, max + 1):
                score = 0
                S = self.env.reset()
                P = self._epsilon_greedy_probs(Q[S], idx, 0.005)
                while True:
                    A = np.random.choice(np.arange(self.nA), p=P)
                    S_prime, R, done, info = self.env.step(A)
                    score += R
                    P = self._epsilon_greedy_probs(Q[S_prime], idx, 0.005)
                    Q[S][A] = self._update_Q(Q[S][A], np.dot(Q[S_prime], P), R, alpha, gamma)
                    S = S_prime
                    if done:
                        scores.append(score)
                        break
                if (idx % PLOT_EVERY == 0):
                    series.append(np.mean(scores))
                
                bar.update(bar.value + 1)
            
            data.append(series)
            bar.finish()
        
            #P = np.array([np.argmax(Q[key]) if key in Q else -1 for key in np.arange(48)]).reshape(4,12)
            #run_check('td_control_check', P)
        
            if plot:
                title = 'Expected SARSA (alpha = %.2f, gamma = %.2f)' % (alpha, gamma)
                output = self._build_output_filename('expected_sarsa-performance')
                plot_performance(title, data, output)
                V = ([np.max(Q[key]) if key in Q else 0 for key in np.arange(48)])
                plot_values(V, self._build_output_filename('expected_sarsa-values', max))

    def run(self):
        (sarsa, q_learning, expected_sarsa) = (self.args.sarsa, self.args.q_learning, self.args.expected_sarsa)
        if sarsa:
            self._sarsa()
        elif q_learning:
            self._q_learning()
        elif expected_sarsa:
            self._expected_sarsa()
        else:
            raise ValueError()
#endregion