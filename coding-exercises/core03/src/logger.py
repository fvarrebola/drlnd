# -*- coding:utf-8 -*-
import logging

logging.basicConfig(\
    format= '%(asctime)-15s %(levelname)-8s %(message)s',
    handlers=[
        logging.FileHandler('/var/tmp/drlnd.log'),
        logging.StreamHandler()
    ])
LOGGER = logging.getLogger()
LOGGER.setLevel('INFO')

CRITICAL = logging._nameToLevel['CRITICAL']
DEBUG = logging._nameToLevel['DEBUG']
INFO = logging._nameToLevel['INFO']
WARN =  logging._nameToLevel['WARNING']

def _log(msg, level=INFO):
    LOGGER.log(level, msg)

def is_debug_enabled():
    return LOGGER.isEnabledFor(DEBUG)

def info(msg):
    _log(msg, level=INFO)

def debug(msg):
    _log(msg, level=DEBUG)

def warn(msg):
    _log(msg, level=WARN)

def fatal(msg):
    _log(msg, level=CRITICAL)