# -*- coding:utf-8 -*-
import argparse
import gym
import numpy as np
import signal
import sys
import torch

import logger
import ch02
import ch03

# region sigint_handler
def signal_handler(sig, frame):
    logger.info("Exiting")
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
#endregion

def _make_gym_env(name):
    ''' Initializes the environment '''
    env = gym.make(name)
    
    env.seed(0)
    np.random.seed(0)
    torch.manual_seed(0)
    
    logger.info('Environment \'%s\'' % name)
    logger.info('Observation space: %s' % env.observation_space)
    logger.info('Action space: %s' % env.action_space)
    
    return env

#region main
def main(parser):
    ''' Entry point '''
    args = parser.parse_args()
    if args.cmd == 'ce':
        exercise = ch02.MountainCarExercise(_make_gym_env('MountainCarContinuous-v0'), args)
    elif args.cmd == 'hc':
        exercise = ch02.CartPoleExercise(_make_gym_env('CartPole-v0'), args)
    elif args.cmd == 'reinforce':
        exercise = ch03.CartPoleExercise(_make_gym_env('CartPole-v0'), args)
    else:
        parser.print_help()
        return
    exercise.run()


def _build_argparser():
    
    parser = argparse.ArgumentParser(description='DRLND coding exercises (Policy-Based Methods).\n')
    parser.add_argument('--episodes', type=int, default=1000, help='episodes (defaults to 1000)')
    parser.add_argument('--gamma', type=float, default=1.0, help='gamma discounting factor (defaults to 1.0)')
    parser.add_argument('--seed', type=int, default=1, help='seed (defaults to 1)')
    parser.add_argument('--plot', action='store_true', help='plot graphs (defaults to false)')
    parser.add_argument('--model', type=str, default='model.pt', help='checkpoint file name (defaults to \'model.pt\')')

    cmd_sp = parser.add_subparsers(dest='cmd')
    ce_p = cmd_sp.add_parser('ce', help='runs cross-entropy method in MountainCar environment')
    ce_grp = ce_p.add_mutually_exclusive_group(required=True)
    ce_grp.add_argument('-train', action='store_true', help='train the agent')
    ce_grp.add_argument('-watch', action='store_true', help='watch the agent interact')
    
    hc_p = cmd_sp.add_parser('hc', help='runs hill climbing in CartPole environment')
    hc_grp = hc_p.add_mutually_exclusive_group(required=True)
    hc_grp.add_argument('-train', action='store_true', help='train the agent')
    hc_grp.add_argument('-watch', action='store_true', help='watch the agent interact')

    re_p = cmd_sp.add_parser('reinforce', help='runs REINFORCE in CartPole environment')
    re_grp = re_p.add_mutually_exclusive_group(required=True)
    re_grp.add_argument('-train', action='store_true', help='train the agent')
    re_grp.add_argument('-watch', action='store_true', help='watch the agent interact')
    
    return parser



if __name__ == "__main__":
    main(_build_argparser())
    sys.exit(0)
#endregion