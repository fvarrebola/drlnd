# -*- coding:utf-8 -*-
import os

def check_not_null(*args):
    assert (len(args) > 0)
    for arg in args:
        assert (arg is not None) 

def check_not_empty(*args):
    assert (len(args) > 0)
    for arg in args:
        check_not_null(arg)
        assert (len(arg) > 0) 

def check(condition):
    check_not_null(condition)
    assert (condition)

def exists(*args):
    assert (len(args) > 0)
    for arg in args:
        check_not_null(arg)
        assert (os.path.exists(arg))