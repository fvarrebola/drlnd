# -*- coding:utf-8 -*-
import argparse
import gym
import logger
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import sys
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

from collections import defaultdict, deque
from params import check_not_null
from progressbar import ProgressBar
from pyvirtualdisplay import Display
from torch.autograd import Variable
from torch.distributions import Categorical

USE_PYVDISPLAY = (True if os.name == 'posix' else False)
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

plt.ion()

#region exercises
class CartPoleExercise:
    
    SCORES_WINDOW = 100
    MAX_STEPS = 1000
    MIN_SCORE = 195.0
    
    class Policy(nn.Module):
        def __init__(self, s_size=4, h_size=16, a_size=2):
            super(CartPoleExercise.Policy, self).__init__()
            self.fc1 = nn.Linear(s_size, h_size)
            self.fc2 = nn.Linear(h_size, a_size)

        def forward(self, x):
            x = F.relu(self.fc1(x))
            x = self.fc2(x)
            return F.softmax(x, dim=1)
        
        def act(self, state):
            state = torch.from_numpy(state).float().unsqueeze(0).to(DEVICE)
            probs = self.forward(state).cpu()
            m = Categorical(probs)
            action = m.sample()
            return action.item(), m.log_prob(action)
    
    def __init__(self, env, args):
        check_not_null(env)
        self.env = env
        self.args = args
    
    def _plot(self, scores, title, output):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        plt.plot(np.arange(1, len(scores)+1), scores)
        plt.ylabel('Score')
        plt.xlabel('Episode #')
        plt.grid(True, which='major', linewidth=0.25)
        plt.grid(True, which='minor', linewidth=0.1)
        plt.title(title)
        plt.savefig(output)
    
    def _train(self):
        
        (n_episodes, gamma, model, plot) = \
            (self.args.episodes, self.args.gamma, self.args.model, self.args.plot)
        
        policy = CartPoleExercise.Policy().to(DEVICE)
        optimizer = optim.Adam(policy.parameters(), lr=1e-2)
        
        scores_deque = deque(maxlen=CartPoleExercise.SCORES_WINDOW)
        scores = []
        
        msgs = []
        msgs.append("Last %d scores" % CartPoleExercise.SCORES_WINDOW)
        msgs.append("#   \tavg score")
        msgs.append("----\t---------")
        
        bar = ProgressBar(max_value=n_episodes, redirect_stdout=True)
        for idx in range(1, n_episodes + 1):
            saved_log_probs = []
            rewards = []
            state = self.env.reset()
            for t in range(CartPoleExercise.MAX_STEPS):
                action, log_prob = policy.act(state)
                saved_log_probs.append(log_prob)
                state, reward, done, _ = self.env.step(action)
                rewards.append(reward)
                if done:
                    break
            
            rewards_sum = sum(rewards)
            scores_deque.append(rewards_sum)
            scores.append(rewards_sum)
            
            discounts = [gamma**i for i in range(len(rewards)+1)]
            R = sum([a*b for a,b in zip(discounts, rewards)])
            
            policy_loss = []
            for log_prob in saved_log_probs:
                policy_loss.append(-log_prob * R)
            policy_loss = torch.cat(policy_loss).sum()
            
            optimizer.zero_grad()
            policy_loss.backward()
            optimizer.step()
            
            
            mean_score = np.mean(scores_deque)
            if idx % CartPoleExercise.SCORES_WINDOW == 0:
                msgs.append('%4d\t%.7f' % (idx, mean_score))
            
            if mean_score >= CartPoleExercise.MIN_SCORE:
                msgs.append("Environment solved in %d episodes" % idx)
                msgs.append("Average score %.7f" % mean_score)
                break
            
            bar.update(bar.value + 1)
        
        bar.finish()
        
        for msg in msgs: 
            logger.info(msg)
                
        with open(model, 'wb') as fp:
            logger.info("Saving policy to file '%s'" % model)
            pickle.dump(policy, fp)
        
        if plot: 
            self._plot(scores, "REINFORCE average scores", ("reinforce-scores-e%06d.png" % n_episodes))
    
    def _watch(self):
        
        model = self.args.model
        with open(model, 'rb') as fp:
            logger.info("Loading policy from file '%s'" % model)
            policy = pickle.load(fp)
        
        if USE_PYVDISPLAY:
            display = Display(visible=0, size=(1400, 900))
            display.start()
            img = plt.imshow(self.env.render(mode='rgb_array'))
        else:
            self.env.render(mode='rgb_array')
        
        logger.info("Watching agent interact with the environment... press CTRL+C to stop it")
        episode = 0
        while True:
            S = self.env.reset()
            steps = 0
            while True:
                A, _ = policy.act(S)
                if USE_PYVDISPLAY:
                    img.set_data(self.env.render(mode='rgb_array')) 
                    plt.axis('off')
                    display.display(plt.gcf())
                    display.clear_output(wait=True)
                else:
                    self.env.render(mode='rgb_array')
                
                S, R, done, _ = self.env.step(A)
                steps += 1
                if done:
                    logger.info("Episode #%04d ended int %04d steps with reward %.2f" % (episode + 1, steps, R))
                    break 
            episode += 1
    
    def run(self):
        (train, watch) = (self.args.train, self.args.watch)
        if train:
            self._train()
        elif watch:
            self._watch()
        else:
            raise ValueError()
        
        if self.env:
            self.env.close()
#endregion