# -*- coding:utf-8 -*-
import argparse
import gym
import logger
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import sys
import torch
import torch.nn as nn
import torch.nn.functional as F

from collections import defaultdict, deque
from params import check_not_null
from progressbar import ProgressBar
from pyvirtualdisplay import Display
from torch.autograd import Variable

USE_PYVDISPLAY = (True if os.name == 'posix' else False)
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

plt.ion()

#region exercises
class MountainCarExercise:
    
    SCORES_WINDOW = 10
    MAX_STEPS = 1000
    MIN_SCORE = 100.0
    
    class Agent(nn.Module):
        def __init__(self, env, h_size=16):
            super(MountainCarExercise.Agent, self).__init__()
            self.env = env
            # state, hidden layer, action sizes
            self.s_size = env.observation_space.shape[0]
            self.h_size = h_size
            self.a_size = env.action_space.shape[0]
            # define layers
            self.fc1 = nn.Linear(self.s_size, self.h_size)
            self.fc2 = nn.Linear(self.h_size, self.a_size)
            
        def set_weights(self, weights):
            s_size = self.s_size
            h_size = self.h_size
            a_size = self.a_size
            # separate the weights for each layer
            fc1_end = (s_size*h_size)+h_size
            fc1_W = torch.from_numpy(weights[:s_size*h_size].reshape(s_size, h_size))
            fc1_b = torch.from_numpy(weights[s_size*h_size:fc1_end])
            fc2_W = torch.from_numpy(weights[fc1_end:fc1_end+(h_size*a_size)].reshape(h_size, a_size))
            fc2_b = torch.from_numpy(weights[fc1_end+(h_size*a_size):])
            # set the weights for each layer
            self.fc1.weight.data.copy_(fc1_W.view_as(self.fc1.weight.data))
            self.fc1.bias.data.copy_(fc1_b.view_as(self.fc1.bias.data))
            self.fc2.weight.data.copy_(fc2_W.view_as(self.fc2.weight.data))
            self.fc2.bias.data.copy_(fc2_b.view_as(self.fc2.bias.data))
        
        def get_weights_dim(self):
            return (self.s_size+1)*self.h_size + (self.h_size+1)*self.a_size
            
        def forward(self, x):
            x = F.relu(self.fc1(x))
            x = torch.tanh(self.fc2(x))
            return x.cpu().data
            
        def evaluate(self, weights, gamma=1.0, max_t=5000):
            self.set_weights(weights)
            episode_return = 0.0
            state = self.env.reset()
            for t in range(max_t):
                state = torch.from_numpy(state).float().to(DEVICE)
                action = self.forward(state)
                state, reward, done, _ = self.env.step(action)
                episode_return += reward * math.pow(gamma, t)
                if done:
                    break
            return episode_return

    def __init__(self, env, args):
        check_not_null(env)
        self.env = env
        self.agent = MountainCarExercise.Agent(env).to(DEVICE)
        self.args = args
    
    def _plot(self, scores, title, output):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        plt.plot(np.arange(1, len(scores)+1), scores)
        plt.ylabel('Score')
        plt.xlabel('Episode #')
        plt.grid(True, which='major', linewidth=0.25)
        plt.grid(True, which='minor', linewidth=0.1)
        plt.title(title)
        plt.savefig(output)
        plt.clf()
    
    def _train(self):
        
        pop_size = 50
        elite_frac = 0.2
        sigma = 0.5
        
        (n_episodes, gamma, model, plot) = \
            (self.args.episodes, self.args.gamma, self.args.model, self.args.plot)
        
        n_elite=int(pop_size*elite_frac)
        
        scores_deque = deque(maxlen=100)
        scores = []
        best_weight = sigma*np.random.randn(self.agent.get_weights_dim())
        
        msgs = []
        msgs.append("Last %d scores" % CartPoleExercise.SCORES_WINDOW)
        msgs.append("#   \tavg score")
        msgs.append("----\t---------")
        
        bar = ProgressBar(max_value=n_episodes, redirect_stdout=True)
        for idx in range(1, n_episodes + 1):
            weights_pop = [best_weight + (sigma*np.random.randn(self.agent.get_weights_dim())) for i in range(pop_size)]
            rewards = np.array([self.agent.evaluate(weights, gamma, MountainCarExercise.MAX_STEPS) for weights in weights_pop])

            elite_idxs = rewards.argsort()[-n_elite:]
            elite_weights = [weights_pop[i] for i in elite_idxs]
            best_weight = np.array(elite_weights).mean(axis=0)

            reward = self.agent.evaluate(best_weight, gamma=1.0)
            scores_deque.append(reward)
            scores.append(reward)
            
            torch.save(self.agent.state_dict(), model)
            
            mean_score = np.mean(scores_deque)
            if idx % MountainCarExercise.SCORES_WINDOW == 0:
                msgs.append('%4d\t%.7f' % (idx, mean_score))
            
            if mean_score >= MountainCarExercise.MIN_SCORE:
                msgs.append("Environment solved in %d episodes" % idx)
                msgs.append("Average score %.7f" % mean_score)
                break
            
            bar.update(bar.value + 1)
        
        bar.finish()
        
        for msg in msgs: 
            logger.info(msg)
                
        if plot: 
            self._plot(scores, "Cross-entropy scores", ("ce-scores-e%06d.png" % n_episodes))

    def _watch(self):
        
        logger.info("Loading model from file '%s'" % self.args.model)
        self.agent.load_state_dict(torch.load(self.args.model))

        if USE_PYVDISPLAY:
            display = Display(visible=0, size=(1400, 900))
            display.start()
            img = plt.imshow(self.env.render(mode='rgb_array'))
        else:
            self.env.render(mode='rgb_array')
        
        logger.info("Watching agent interact with the environment... press CTRL+C to stop it")
        episode = 0
        while True:
            S = self.env.reset()
            steps = 0
            while True:
                S = torch.from_numpy(S).float().to(DEVICE)
                with torch.no_grad():
                    A = self.agent(S)
                if USE_PYVDISPLAY:
                    img.set_data(self.env.render(mode='rgb_array')) 
                    plt.axis('off')
                    display.display(plt.gcf())
                    display.clear_output(wait=True)
                else:
                    self.env.render(mode='rgb_array')
                
                S_prime, R, done, _ = self.env.step(A)
                S = S_prime
                steps += 1
                if done:
                    logger.info("Episode #%04d ended int %04d steps with reward %.2f" % (episode + 1, steps, R))
                    break
            episode += 1
    
    
    def run(self):
        (train, watch) = (self.args.train, self.args.watch)
        if train:
            self._train()
        elif watch:
            self._watch()
        else:
            raise ValueError()
        
        if self.env:
            self.env.close()

class CartPoleExercise:

    SCORES_WINDOW = 100
    MAX_STEPS = 1000
    MIN_SCORE = 300.0

    class Policy():
        def __init__(self, s_size=4, a_size=2):
            self.w = 1e-4*np.random.rand(s_size, a_size)  # weights for simple linear policy: state_space x action_space
            
        def forward(self, state):
            x = np.dot(state, self.w)
            return np.exp(x)/sum(np.exp(x))
        
        def act(self, state):
            probs = self.forward(state)
            #action = np.random.choice(2, p=probs) # option 1: stochastic policy
            action = np.argmax(probs)              # option 2: deterministic policy
            return action
            
    def __init__(self, env, args):
        check_not_null(env)
        self.env = env
        self.args = args
    
    def _plot(self, scores):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        plt.plot(np.arange(1, len(scores)+1), scores)
        plt.ylabel('Score')
        plt.xlabel('Episode #')
        plt.show()
    
    def _train(self):
        
        noise_scale=1e-2
        
        (n_episodes, gamma, model, plot) = \
            (self.args.episodes, self.args.gamma, self.args.model, self.args.plot)
        
        policy = CartPoleExercise.Policy()

        scores_deque = deque(maxlen=100)
        scores = []
        best_R = -np.Inf
        best_w = policy.w
        
        msgs = []
        msgs.append("Last %d scores" % CartPoleExercise.SCORES_WINDOW)
        msgs.append("#   \tavg score")
        msgs.append("----\t---------")
        
        bar = ProgressBar(max_value=n_episodes, redirect_stdout=True)
        for idx in range(1, n_episodes + 1):
            R_list = []
            S = self.env.reset()
            for t in range(CartPoleExercise.MAX_STEPS):
                A = policy.act(S)
                S, R, done, _ = self.env.step(A)
                R_list.append(R)
                if done:
                    break 
            
            R_sum = sum(R_list)
            scores_deque.append(R_sum)
            scores.append(R_sum)

            discounts = [gamma**i for i in range(len(R_list)+1)]
            R = sum([a*b for a,b in zip(discounts, R_list)])

            if R >= best_R: # found better weights
                best_R = R
                best_w = policy.w
                noise_scale = max(1e-3, noise_scale / 2)
                policy.w += noise_scale * np.random.rand(*policy.w.shape) 
            else: # did not find better weights
                noise_scale = min(2, noise_scale * 2)
                policy.w = best_w + noise_scale * np.random.rand(*policy.w.shape)

            mean_score = np.mean(scores_deque)
            if idx % CartPoleExercise.SCORES_WINDOW == 0:
                msgs.append('%4d\t%.7f' % (idx, mean_score))
            
            if mean_score >= CartPoleExercise.MIN_SCORE:
                msgs.append("Environment solved in %d episodes" % idx)
                msgs.append("Average score %.7f" % mean_score)
                policy.w = best_w
                break
            
            bar.update(bar.value + 1)
        
        bar.finish()
        
        for msg in msgs: 
            logger.info(msg)
                
        with open(model, 'wb') as fp:
            logger.info("Saving policy to file '%s'" % model)
            pickle.dump(policy, fp)
        
        if plot: 
            self._plot(scores, "Hill-climbing algorithm scores", ("hc-scores-e%06d.png" % n_episodes))
    
    def _watch(self):
        
        model = self.args.model
        with open(model, 'rb') as fp:
            logger.info("Loading policy from file '%s'" % model)
            policy = pickle.load(fp)
        
        if USE_PYVDISPLAY:
            display = Display(visible=0, size=(1400, 900))
            display.start()
            img = plt.imshow(self.env.render(mode='rgb_array'))
        else:
            self.env.render(mode='rgb_array')
        
        logger.info("Watching agent interact with the environment... press CTRL+C to stop it")
        episode = 0
        while True:
            S = self.env.reset()
            steps = 0
            while True:
                A = policy.act(S)
                if USE_PYVDISPLAY:
                    img.set_data(self.env.render(mode='rgb_array')) 
                    plt.axis('off')
                    display.display(plt.gcf())
                    display.clear_output(wait=True)
                else:
                    self.env.render(mode='rgb_array')
                
                S, R, done, _ = self.env.step(A)
                steps += 1
                if done:
                    logger.info("Episode #%04d ended int %04d steps with reward %.2f" % (episode + 1, steps, R))
                    break 
            episode += 1
    
    def run(self):
        (train, watch) = (self.args.train, self.args.watch)
        if train:
            self._train()
        elif watch:
            self._watch()
        else:
            raise ValueError()
        
        if self.env:
            self.env.close()
#endregion