# Project details

In this part of the repository you will find unit 3 (Policy-Based Methods) coding exercises.

# Getting started

All you have to do is to create a Python virtual environment, activate it and install the requirements using `pip`.
```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

Please note that if you are running on Windows you may have to install [PyTorch](https://pytorch.org) manually according to the instructions found [here](https://pytorch.org/get-started/locally/).

# Instructions

Once your environment is set, navigate to folder `src/` run `python main.py`. 

You should see the following output:
```
usage: main.py [-h] [--episodes EPISODES] [--gamma GAMMA] [--seed SEED]
               [--plot] [--model MODEL]
               {ce,hc,reinforce} ...

DRLND coding exercises (Policy-Based Methods).

positional arguments:
  {ce,hc,reinforce}
    ce                 runs cross-entropy method in MountainCar environment
    hc                 runs hill climbing in CartPole environment
    reinforce          runs REINFORCE in CartPole environment

optional arguments:
  -h, --help           show this help message and exit
  --episodes EPISODES  episodes (defaults to 1000)
  --gamma GAMMA        gamma discounting factor (defaults to 1.0)
  --seed SEED          seed (defaults to 1)
  --plot               plot graphs (defaults to false)
  --model MODEL        checkpoint file name (defaults to 'model.pt')
```


## Available algorithms and actions

All the positional arguments `ce`, `hc` and `reinforce` represent an algorithm and support actions `train` and `watch`. 

### Training the model

Once you chose an algorithm, derive a policy using `python main.py {algorithm} -train`.

For instance, if you run `python main.py reinforce -train` you should see an output like the following:
```
2019-03-30 17:54:43,065 INFO     Environment 'CartPole-v0'
2019-03-30 17:54:43,066 INFO     Observation space: Box(4,)
2019-03-30 17:54:43,066 INFO     Action space: Discrete(2)
100% (1000 of 1000) |####################| Elapsed Time: 0:01:24 Time:  0:01:24
2019-03-30 17:56:07,834 INFO     Last 100 scores
2019-03-30 17:56:07,835 INFO     #      avg score
2019-03-30 17:56:07,835 INFO     ----   ---------
2019-03-30 17:56:07,836 INFO      100   34.4700000
2019-03-30 17:56:07,836 INFO      200   66.2600000
2019-03-30 17:56:07,836 INFO      300   87.8200000
2019-03-30 17:56:07,836 INFO      400   72.8300000
2019-03-30 17:56:07,837 INFO      500   172.0000000
2019-03-30 17:56:07,837 INFO      600   160.6500000
2019-03-30 17:56:07,837 INFO      700   167.1500000
2019-03-30 17:56:07,838 INFO     Environment solved in 791 episodes
2019-03-30 17:56:07,838 INFO     Average score 196.6900000
2019-03-30 17:56:07,839 INFO     Saving policy to file 'model.pt'
```

Training saves the resulting model to `model.pt` file. You can use the `--model` flag to override this behavior.

Training also plots scores to `[action]-scores-e[#-of-episodes].png` where:
- `action` indicates an action
- `#-of-episodes` indicates the number of episodes (that defaults to `1000`)

A typical `reinforce` plot would look like this:

![Plot of policy derived using REINFORCE after 1000 episodes.](img/reinforce-scores-e001000.png)

### Watching the agent

If you would like to watch the agent interact with the environment use action run `python main.py --model reinforce.pt -watch`.

After a while you should see an output like:

```
2019-03-30 18:31:36,321 INFO     Environment 'CartPole-v0'
2019-03-30 18:31:36,322 INFO     Observation space: Box(4,)
2019-03-30 18:31:36,326 INFO     Action space: Discrete(2)
2019-03-30 18:31:36,332 INFO     Loading policy from file 'reinforce.pt'
2019-03-30 18:31:39,123 INFO     Watching agent interact with the environment... press CTRL+C to stop it
2019-03-30 18:31:44,114 INFO     Episode #0001 ended int 0200 steps with reward 1.00
...
```

You should also see a display like the following:

![REINFORCE agent interacting with the environment.](img/reinforce-agent-interaction.gif)