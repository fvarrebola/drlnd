# Introduction

This repository contains all coding assignments from [Udacity](https://www.udacity.com)'s Deep Reinforcement Learning Nanodegree. 

There are two main folders:

1. The `coding-exercices/` folder which contains core curriculum coding exercises:
    - Introduction to Deep Reinforcement (`coding-exercices/core01`)
    - Value-Based Methods (`coding-exercices/core02`)
    - Policy-Based Methods (`coding-exercices/core03`)
    - Multi-Agent Reinforcement (`coding-exercices/core04`)
        
    and extracurricular coding exercises: 
        
2. The `projects/` folder which contains:
    - Banana collector navigation project (`projects/navigation/`)
    - Double-jointed arm project (`projects/continuous/`)
    - Tennis players (`projects/tennis/`)

# Getting started

Clone the repository using your preffered `git` client:
```
git clone https://www.gitlab.com/fvarrebola/drlnd.git
```

# Instructions

Follow the instructions of the `README.md` file in the folder of your preference.
